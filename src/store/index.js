import VueX from 'vuex';
import Vue from 'vue';
import todos from './modules/todos';

Vue.use(VueX);

export default new VueX.Store({
    modules: {
        todos
    }
});